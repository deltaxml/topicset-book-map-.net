# Topicset Book Map Sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DITA-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the exe files.*

*For example `DeltaXML-DITA-Compare-8_0_0_n/samples/sample-name`.*

---
## Script Features
This sample script compares two DITA Book Maps, book1/main.ditamap and book2/main.ditamap, using the 'topic set', 'map pair' and 'unified map' map result structures. For concept details see: [Topicset Book Map documentation](https://docs.deltaxml.com/dita-compare/latest/topicset-book-map-sample-3801194.html).

### Result Origin
The 'result origin' is the input map that is used to form the basis of the output, as discussed in Map LeveL Output Formats Section of the Reference and the map-result-origin parameter documentation.

### Output Structure and Destination
The output of the 'topic set', 'map pair' and 'unified-map' comparisons are put into the topic-set-A, map-pair-B and unified-map-B directories respectively. These directories contain a result-alias.ditamap file whose content references the actual results, but can be used as a result in its own right. Note that in the case of the map pair result, the alias combines the two main result maps into a single document.

## Using a Batch File

Run the rundemo.bat batch file either by entering rundemo from the command-line or by double-clicking on this file from Windows Explorer. This script runs the comparison twice, once for a 'topic-set' and once for a 'unified map'.

## Running the sample from the Command line

The sample comparison can also be run from the command line, for example, the 'topic-set' comparison is performed using the following:

    ..\..\bin\deltaxml-dita.exe compare map book1/main.ditamap book2/main.ditamap topic-set-A map-result-origin=A map-result-structure=topic-set

The 'map pair' or 'unified map' comparisons can be run in a similar manner to the 'topic set', but in this case the map-result-orign is B, the map-result-structure is map-pair or unified-map, and the map-copy-location is map-pair-B or unified-map-B.