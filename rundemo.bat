echo off
cd %CD%
set path=%path%;%SystemRoot%\Microsoft.NET\Framework\v3.5
echo clearing previous output
IF EXIST unified-map-b (
rmdir unified-map-b /s/q
)
IF EXIST topic-set-a (
rmdir topic-set-a /s/q
)
echo starting 1 of 2
..\..\bin\deltaxml-dita.exe compare map book1/main.ditamap book2/main.ditamap topic-set-A map-result-origin=A map-result-structure=topic-set
echo starting 2 of 2
..\..\bin\deltaxml-dita.exe compare map book1/main.ditamap book2/main.ditamap unified-map-B map-result-origin=B map-result-structure=unified-map
echo complete
pause
